const Serial = funcs =>
    funcs.reduce(
        (promise, func) =>
            promise.then(result => func().then(Array.prototype.concat.bind(result))),
        Promise.resolve([])
    );
const Chain = funcs => {
    let results = [];
    return funcs.reduce(
        (promise, func) =>
            promise.then(result => {
                return func().then(result => {
                    results.push(result);
                    return results;
                });
            }),
        Promise.resolve([])
    );
};

export {Serial, Chain};
